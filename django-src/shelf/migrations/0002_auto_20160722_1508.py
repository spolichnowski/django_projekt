# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-07-22 13:08
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shelf', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='director',
            options={'ordering': ('-id',), 'verbose_name': 'Re\u017cysera', 'verbose_name_plural': 'Re\u017cyserzy'},
        ),
        migrations.AlterModelOptions(
            name='movie',
            options={'ordering': ('-id',), 'verbose_name': 'Film', 'verbose_name_plural': 'Filmy'},
        ),
        migrations.AlterModelOptions(
            name='writer',
            options={'ordering': ('-name',), 'verbose_name': 'Scenarzysta', 'verbose_name_plural': 'Scenarzy\u015bci'},
        ),
        migrations.AlterField(
            model_name='director',
            name='last_name',
            field=models.CharField(max_length=120, verbose_name='Nazwisko'),
        ),
        migrations.AlterField(
            model_name='director',
            name='name',
            field=models.CharField(max_length=80, verbose_name='Imi\u0119'),
        ),
        migrations.AlterField(
            model_name='movie',
            name='director',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='shelf.Director', verbose_name='Dyrektor'),
        ),
        migrations.AlterField(
            model_name='movie',
            name='rate',
            field=models.CharField(max_length=20, verbose_name='Ocena'),
        ),
        migrations.AlterField(
            model_name='movie',
            name='title',
            field=models.CharField(max_length=100, verbose_name='Tytu\u0142'),
        ),
        migrations.AlterField(
            model_name='movie',
            name='writer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='shelf.Writer', verbose_name='Senarzysta'),
        ),
        migrations.AlterField(
            model_name='writer',
            name='last_name',
            field=models.CharField(max_length=120, verbose_name='Nazwisko'),
        ),
        migrations.AlterField(
            model_name='writer',
            name='name',
            field=models.CharField(max_length=80, verbose_name='Imi\u0119'),
        ),
    ]
