from django.contrib import admin

from .models import Writer, Director, Movie
# Register your models here.



class WriterAdmin(admin.ModelAdmin):
    search_fields = ['name', 'last_name']
    ordering = ['last_name']


class MovieAdmin(admin.ModelAdmin):
    search_fields = ['title']
    list_display = ['title', 'writer', 'rate']


class DirectorAdmin(admin.ModelAdmin):
    search_field = ['name', 'last_name']
    ordering = ['last_name']

admin.site.register(Movie, MovieAdmin)

admin.site.register(Writer, WriterAdmin)

admin.site.register(Director, DirectorAdmin)
