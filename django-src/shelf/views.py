from django.shortcuts import render
from django.views.generic import ListView, DetailView

from shelf.models import Writer



class WriterListView(ListView):
    model = Writer
    context_object_name = 'writers'

class WriterDetailView(DetailView):
    model = Writer
    # context_object_name = 'writer'
