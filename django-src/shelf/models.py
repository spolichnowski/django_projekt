# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models


class Writer(models.Model):
    name = models.CharField('Imię', max_length=80) 
    last_name = models.CharField('Nazwisko', max_length=120)
    
    class Meta:
        app_label = 'shelf'
        ordering = ('-name', )
        verbose_name = 'Scenarzysta'
        verbose_name_plural = 'Scenarzyści'
    
    def __str__(self):
        return '{} {}'.format(self.name, self.last_name)


class Director(models.Model):
    name = models.CharField('Imię', max_length=80)
    last_name = models.CharField('Nazwisko', max_length=120)

    class Meta:
        app_label = 'shelf'
        ordering = ('-id', )
        verbose_name = 'Reżysera'
        verbose_name_plural = 'Reżyserzy'

    def __str__(self):
        return '{name} {last_name}'.format(name=self.name, last_name=self.last_name)    


class Movie(models.Model):
    title = models.CharField('Tytuł', max_length=100)
    writer = models.ForeignKey(Writer, verbose_name='Senarzysta')
    director = models.ForeignKey(Director, verbose_name='Dyrektor')
    rate = models.CharField('Ocena', max_length=20)
    
    class Meta:
        app_label = 'shelf'
        ordering = ('-id', )
        verbose_name = 'Film'
        verbose_name_plural = 'Filmy'

    def __str__(self):
        return self.title

    def get_name_character_numer(self):
        return len(self.title)



    







