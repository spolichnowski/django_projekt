from django.conf.urls import include, url
from django.contrib import admin

from shelf.views import WriterListView
from shelf.views import WriterDetailView

from contacts.views import MessageAddView

from newsletter.views import SignUpView



urlpatterns = [
    # Examples:
    #url(r'^$', 'newsletter.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'newsletter.views.index_view', name='main-page'),
    url(r'newsletter/$', SignUpView.as_view()),
    url(r'writers/$', WriterListView.as_view(), name='writer_list'),
    url(r'writers/(?P<pk>\d+)/$', WriterDetailView.as_view(), name='writer_detail'),
    url(r'contacts/$', MessageAddView.as_view()),
]
