from django.shortcuts import render
from django.views.generic import FormView, TemplateView
from .forms import SignUpForm


class MainPageView(TemplateView):
    template_name = 'templates/index.html'

index_view = MainPageView.as_view()
    

class SignUpView(FormView):
    form_class = SignUpForm
    template_name = 'newsletter/newsletter_view.html'
    

    def form_valid(self, form):
        form.save() 
        return super(FormView, self).form_valid(form)
   























