from django.shortcuts import render
from django.views.generic import FormView

from .forms import MessageForm
from .models import Message



# class MessageDetailView(DetailView):
#    model = Message

class MessageAddView(FormView):
    form_class = MessageForm
    template_name = 'contacts/contact_add.html'
    success_url = '/writers/'

    def form_valid(self, form):
        form.save() 
        return super(MessageView, self).form_valid(form)
